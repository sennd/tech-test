import React from 'react';
import { NextToGoContainer } from './src/containers';

const App = () => (
  <NextToGoContainer />
);

export default App;
