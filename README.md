# Neds.

React-Native technical task from Neds.

This application pulls from the following API:

https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=10

# How to get working:

git clone the repo. 
1. `npm install`
2. `npm start`
3. Load/Build project into expo app.