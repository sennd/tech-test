import { Picker } from '@react-native-picker/picker';
import React, { useState } from 'react';
import {
  FlatList, SafeAreaView, StatusBar, StyleSheet, Text, View,
} from 'react-native';
import { GREYHOUND_CATEGORY_ID, HARNESS_CATEGORY_ID, HORSE_CATEGORY_ID } from '../../config';
import { Loading, Timer } from '../../elements';
import { useNextToGo } from '../../hooks';
import { filterByCategory } from '../../hooks/use-next-to-go/functions/filter-by-category';
import { RaceSummary } from '../../hooks/use-next-to-go/use-next-to-go.types';

export const NextToGoContainer = () => {
  const { summaries, loading } = useNextToGo(10);
  const [selectedCategory, setSelectedCategory] = useState<string>('');

  if (loading) {
    return (
      <Loading />
    );
  }

  const renderItem = ({
    item: {
      raceId, meetingName, raceNumber, secondsTilStart,
    },
  }: { item: RaceSummary }) => (
    <View style={styles.item} key={raceId}>
      <Text style={styles.itemText}>
        {meetingName}
      </Text>
      <Text style={styles.itemText}>
        {raceNumber}
      </Text>
      <Timer time={secondsTilStart} />
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <Picker
        selectedValue={selectedCategory}
        onValueChange={(itemValue) => {
          setSelectedCategory(itemValue as string);
        }}
      >
        <Picker.Item label="All" />
        <Picker.Item label="GreyHound" value={GREYHOUND_CATEGORY_ID} />
        <Picker.Item label="Harness" value={HARNESS_CATEGORY_ID} />
        <Picker.Item label="Horse" value={HORSE_CATEGORY_ID} />
      </Picker>
      <FlatList
        data={filterByCategory(summaries, selectedCategory)}
        renderItem={renderItem}
        keyExtractor={(item) => item.raceId}
      />
    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    marginVertical: 8,
    marginHorizontal: 20,
    padding: 20,
    backgroundColor: '#f9c2ff',
  },
  itemText: {
    flex: 1,
  },
  title: {
    fontSize: 32,
  },
});
