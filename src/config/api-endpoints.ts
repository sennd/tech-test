// @ts-ignore react-native-dotenv does not support type declarations for current release
import { NEDS_API_ENDPOINT } from '@env';

export const NEDS_API = NEDS_API_ENDPOINT;
