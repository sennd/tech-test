// @ts-ignore react-native-dotenv does not support type declarations for current release
import { NEDS_GREYHOUND_CATEGORY_ID, NEDS_HARNESS_CATEGORY_ID, NEDS_HORSE_CATEGORY_ID } from '@env';

export const GREYHOUND_CATEGORY_ID = NEDS_GREYHOUND_CATEGORY_ID || '4a2788f8-e825-4d36-9894-efd4baf1cfae';
export const HARNESS_CATEGORY_ID = NEDS_HARNESS_CATEGORY_ID || '161d9be2-e909-4326-8c2c-35ed71fb460b';
export const HORSE_CATEGORY_ID = NEDS_HORSE_CATEGORY_ID || '9daef0d7-bf3c-4f50-921d-8e818c60fe61';
