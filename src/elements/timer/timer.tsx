/* eslint-disable max-len */
import React, { useState } from 'react';
import { Text, View } from 'react-native';
import { useInterval } from '../../hooks';

type TimerProps = {
    time: number
}

/**
 * countdown timer
 * @param time time in seconds
 */
export const Timer = ({ time }: TimerProps) => {
  const [timeRemaining, setTimeRemaining] = useState(time - Math.round(new Date().getTime() / 1000));

  useInterval(() => {
    setTimeRemaining(timeRemaining - 1);
  }, 1000);

  return (
    <View>
      <Text>
        {timeRemaining}
        s
      </Text>
    </View>
  );
};
