/* eslint-disable no-unused-vars */
import { useEffect, useState } from 'react';
import { NEDS_API } from '../../config';
import { useFetch } from '../use-fetch';
import { transformNextToGoResponse } from './functions/transformNextToGoResponse';
import { NextToGoResponse, RaceSummary } from './use-next-to-go.types';

/**
 *  hook used to retrieve and transform next to go races from neds endpoint
 *  @param count the count of races to retrieve
 * ``` const { data } = useNextToGo(10) ```
 */
export const useNextToGo = (count: number) => {
  const [summaries, setSummaries] = useState<RaceSummary[]>();

  const {
    response, error: fetchError, loading, fetchData,
  } = useFetch<NextToGoResponse>(`${NEDS_API}?method=nextraces&count=${count}`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  });

  useEffect(() => {
    if (response?.status === 200) {
      const {
        summaries: transformedSummaries,
      } = transformNextToGoResponse(response);

      setSummaries(transformedSummaries);
    }
  }, [response, fetchError]);

  return {
    fetchData,
    summaries,
    loading,
  };
};
