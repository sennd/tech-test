/* eslint-disable camelcase */

export type NextToGoResponse = {
  status: number;
  data: {
    next_to_go_ids: string[];
    race_summaries: RaceSummariesResponse
  };
};

export type RaceSummariesResponse = {
  [key: string]: RaceSummaryResponse;
}

export type RaceSummaryResponse = {
  race_id: string;
  race_name: string;
  race_number: string;
  meeting_id: string;
  meeting_name: string;
  category_id: string;
  advertised_start: {
    seconds: string;
  };
  race_form: {
    distance: number;
    distance_type: {
      id: string;
      name: string;
      short_name: string;
    };
    distance_type_id: string;
    track_condition: {
      id: string;
      name: string;
      short_name: string;
    };
    track_condition_id: string;
    weather: {
      id: string;
      name: string;
      short_name: string;
      icon_uri: string;
    };
    weather_id: string;
    additional_data: string;
    generated: number;
    silk_base_url: string;
  };
  venue_id: string;
  venue_name: string;
  venue_state: string;
  venue_country: string;
};

export type RaceSummary = {
  raceId: string
  meetingName: string
  raceNumber: string
  secondsTilStart: number
  categoryId: string
}
