import { NextToGoResponse, RaceSummary } from '../use-next-to-go.types';

export const transformNextToGoResponse = (response: NextToGoResponse) => {
  const summaries: RaceSummary[] = [];

  const raceSumaries = response.data.race_summaries;

  Object.keys(raceSumaries).forEach((key) => {
    const summary = raceSumaries[key];

    const transformedSummary = {
      raceId: summary.race_id,
      categoryId: summary.category_id,
      meetingName: summary.meeting_name,
      raceNumber: summary.race_number,
      secondsTilStart: parseInt(summary.advertised_start.seconds, 10),
    };

    summaries.push(transformedSummary);
  });

  summaries.sort((a, b) => a.secondsTilStart - b.secondsTilStart);

  return {
    ids: response.data.next_to_go_ids,
    summaries,
  };
};
