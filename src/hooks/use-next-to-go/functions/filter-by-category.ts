import { RaceSummary } from '../use-next-to-go.types';

export const filterByCategory = (data?: RaceSummary[], categoryId?: string): RaceSummary[] | null => {
  if (data) {
    if (categoryId) {
      return data.filter((e) => e.categoryId === categoryId);
    }

    return data;
  }

  return null;
};
