import { useEffect, useState } from 'react';

/**
 * Wrapper hook for the http fetch
 * @param url endpoint you want to use
 * @param options fetch options
 *
 * ``` const {fetchDatatch} = useFetch() ```
 */
export const useFetch = <T>(url: RequestInfo, options?: RequestInit) => {
  const [response, setResponse] = useState<T>();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setLoading(true);
    try {
      const res = await fetch(url, options);
      const json = await res.json();
      setResponse(json);
    } catch (e) {
      setError(e);
    }
    setLoading(false);
  };

  return {
    response, error, fetchData, loading,
  };
};
